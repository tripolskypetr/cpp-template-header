#ifndef BASEINFORMER_H
#define BASEINFORMER_H

#include <iostream>
#include <sstream>

template<typename ValueType>
class BaseInformer {
  private:
    int size;
  public:
    BaseInformer();
    virtual ~BaseInformer(){}
  public:
    virtual void printInfo();
    void printValue(ValueType value);
  public:
    std::string getSize() const;
    virtual std::string getName() const = 0;
};

extern template class BaseInformer<int>;
extern template class BaseInformer<char>;

#endif // BASEINFORMER_H
