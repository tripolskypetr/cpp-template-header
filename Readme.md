
# C++ template class with header file

> It looks like C++ template classes can contain methods which are not always inline.

![screenshot](screenshot.png)

BaseInformer.h
```
template<typename ValueType>
class BaseInformer {
  private:
    int size;
  public:
    BaseInformer();
    virtual ~BaseInformer(){}
  public:
    virtual void printInfo();
    void printValue(ValueType value);
  public:
    std::string getSize() const;
    virtual std::string getName() const = 0;
};

extern template class BaseInformer<int>;
extern template class BaseInformer<char>;
```

BaseInformer.cpp
```
template class BaseInformer<int>;
template class BaseInformer<char>;

template<typename ReturnValue>
BaseInformer<ReturnValue>::BaseInformer() {
    size=sizeof(ReturnValue);
}

template<typename ValueType>
void BaseInformer<ValueType>::printValue(ValueType value) {
    std::cout << getName() << " " << value << "\n";
}

template<typename ValueType>
void BaseInformer<ValueType>::printInfo() {
    std::cout << getName() << " " << getSize() << "\n";
}

template<typename ValueType>
std::string BaseInformer<ValueType>::getSize() const {
    std::stringstream ss;
    ss << size;
    return ss.str();
}
```