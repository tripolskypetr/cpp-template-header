#include "baseinformer.h"


template<typename ReturnValue>
BaseInformer<ReturnValue>::BaseInformer() {
    size=sizeof(ReturnValue);
}

template<typename ValueType>
void BaseInformer<ValueType>::printValue(ValueType value) {
    std::cout << getName() << " " << value << "\n";
}

template<typename ValueType>
void BaseInformer<ValueType>::printInfo() {
    std::cout << getName() << " " << getSize() << "\n";
}

template<typename ValueType>
std::string BaseInformer<ValueType>::getSize() const {
    std::stringstream ss;
    ss << size;
    return ss.str();
}

template class BaseInformer<int>;
template class BaseInformer<char>;
