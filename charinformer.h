#ifndef CHARINFORMER_H
#define CHARINFORMER_H

#include "baseinformer.h"

class CharInformer : public BaseInformer<char> {
  public:
    CharInformer(){}
    virtual ~CharInformer() override {}
  public:
    virtual std::string getName() const override;
};


#endif // CHARINFORMER_H
