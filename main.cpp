#include "intinformer.h"
#include "charinformer.h"

int main(int argc, char *argv[]) {

    (void)(argc);
    (void)(argv);

    std::cout << "main func sizeof()\n";
    std::cout << "Int " << sizeof(int) << "\n";
    std::cout << "Char " << sizeof(char) << "\n";

    std::cout << "template printInfo()\n";

    IntInformer().printInfo();
    CharInformer().printInfo();

    std::cout << "template printValue()\n";
    IntInformer().printValue(42);
    CharInformer().printValue('q');

    return 0;
}
