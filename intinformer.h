#ifndef INTINFORMER_H
#define INTINFORMER_H

#include "baseinformer.h"

class IntInformer : public BaseInformer<int> {
  public:
    IntInformer(){}
    virtual ~IntInformer() override {}
  public:
    virtual std::string getName() const override;
};

#endif // INTINFORMER_H
